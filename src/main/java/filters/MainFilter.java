package filters;



import util.AuthorizationToken;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class MainFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("MainFilter init");
    }


    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = ((HttpServletRequest) req).getSession(false);
        if ((session == null)){

            req.setAttribute("logged",false);
        }
        else {
            Object token = session.getAttribute("token");
            if (token != null) {
                try {
                    AuthorizationToken authorizationToken = new AuthorizationToken((String) token);
                    request.setAttribute("logged", true);

                    request.setAttribute("username", authorizationToken.getUsername());
                    request.setAttribute("userId", authorizationToken.getUserId());
                    boolean hasRights = authorizationToken.hasRights();
                    request.setAttribute("admin_panel_enabled", hasRights);
                    if (hasRights) {
                        request.setAttribute("can_block_article", authorizationToken.checkRight(AuthorizationToken.CAN_BLOCK_ARTICLE));
                        request.setAttribute("can_block_users", authorizationToken.checkRight(AuthorizationToken.CAN_BLOCK_ARTICLE));
                        request.setAttribute("can_edit_personnel", authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_PERSONNEL));
                        request.setAttribute("can_edit_tags", authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_TAG));
                        request.setAttribute("can_edit_roles", authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_ROLES));
                    }
                } catch (GeneralSecurityException e) {
                    request.getSession().removeAttribute("token");
                    request.setAttribute("errorMessage", "Problem with security token.Please log in again.");
                    request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
                    //TODO write to logs

                } catch (Exception e) {
                    System.out.println("error from filter");
                    e.printStackTrace();
                }
            }
            else {
                req.setAttribute("logged",false);
            }
        }


        chain.doFilter(request,response);


    }

    @Override
    public void destroy() {
        System.out.println("MainFilter destror");
    }
}
