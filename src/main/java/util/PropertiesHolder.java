package util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHolder {
    private static PropertiesHolder propertiesHolder;

    private  String path;
    private Properties properties;
    private boolean loaded;



    private PropertiesHolder() {
        path = PropertiesHolder.class.getClassLoader().getResource("resource/blog.properties").getPath();
        properties = new Properties();

        load();
    }

    private void load(){
        try(InputStream inputStream = new FileInputStream(path)) {
            properties.load(inputStream);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getDatabaseName(){
        return properties.getProperty("database","blog_chizhevsky");
    }
    public String getDatabaseUser(){
        return properties.getProperty("user","postgres");
    }
    public String getDatabasePassword(){
       return properties.getProperty("password","root");
    }

    public static PropertiesHolder getInstance(){
        if (propertiesHolder == null){
            propertiesHolder = new PropertiesHolder();
            //propertiesHolder.load();
        }
        return propertiesHolder;

    }
    public int getMaxArticlesPerPage(){
        return Integer.parseInt(properties.getProperty("articlesLimit","7"));
    }

    public String getKeyForAES(){
        return properties.getProperty("keyForAES","TheBestSecretKey");
    }

    public int getMaxSearchResults(){
        return Integer.parseInt(properties.getProperty("MaxSearchResults","5"));
    }



}
