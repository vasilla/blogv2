package util;

import com.google.gson.*;
import model.User;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

public class AuthorizationToken {
    public static final int CAN_BLOCK_USER = 1;
    public static final int CAN_EDIT_PERSONNEL = 2;
    public static final int CAN_EDIT_ROLES = 3;
    public static final int CAN_BLOCK_ARTICLE = 4;
    public static final int CAN_EDIT_TAG = 5;

    private JsonObject jsonToken;
    public AuthorizationToken(String encodedToken) throws IllegalBlockSizeException, InvalidKeyException,
            BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        String jsonStr = decryptAES(encodedToken,PropertiesHolder.getInstance().getKeyForAES());
        JsonParser jsonParser = new JsonParser();
        jsonToken = jsonParser.parse(jsonStr).getAsJsonObject();
    }

    public AuthorizationToken(User user, List<Integer> rights){
        Gson gson = new Gson();
        jsonToken = new JsonObject();
        jsonToken.addProperty("username",user.getUsername());
        jsonToken.addProperty("user_id",user.getId());
        jsonToken.add("rights",gson.toJsonTree(rights));
    }

    public String getUsername(){
        return jsonToken.getAsJsonPrimitive("username").getAsString();
    }
    public long getUserId(){
        return jsonToken.getAsJsonPrimitive("user_id").getAsLong();
    }

    public boolean checkRight(int right){
        JsonArray rights = jsonToken.get("rights").getAsJsonArray();
        for (JsonElement jsonElement:
             rights) {
            if (jsonElement.getAsInt() == right){
                return true;
            }
        }
        return false;
    }
    public boolean hasRights(){
        JsonArray rights = jsonToken.get("rights").getAsJsonArray();
        return (rights.size() != 0);
    }

    public boolean checkValid(){
        return (jsonToken.has("username")&&jsonToken.has("user_id")&&
                jsonToken.has("rights"));
    }

    public String toJsonString(){
        return jsonToken.toString();
    }

    public String toEncodedString() throws IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException,
            NoSuchAlgorithmException, BadPaddingException {
        return encryptAES(toJsonString(),PropertiesHolder.getInstance().getKeyForAES());
    }

    public static String encryptAES(String data,String strKey) throws BadPaddingException, IllegalBlockSizeException,
            InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Key key = new SecretKeySpec(strKey.getBytes(),"AES");
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encVal);
    }
    public static String decryptAES(String encryptedData,String strKey) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Key key = new SecretKeySpec(strKey.getBytes(),"AES");
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        return new String(decValue);
    }


}
