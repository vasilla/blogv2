package servlets;

import model.Article;
import service.ArticleService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

//TODO tomcat not in root folder
public class MainPageServlet extends ArticleServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            ArticleService articleService = new ArticleService();
            List<Article> articles = articleService.getPublished(getPage(request,articleService.getCountOfPublished()));
            request.setAttribute("articles",articles);
            request.setAttribute("url_prefix","/");
            request.setAttribute("imagesPath",getImageUrl(request));
            request.getRequestDispatcher("/jsp/main.jsp").forward(request,response);
        } catch (Exception e) {
            handleExeption(request,response,e);
        }

    }
}
