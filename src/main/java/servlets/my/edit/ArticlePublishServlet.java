package servlets.my.edit;

import service.ArticleService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class ArticlePublishServlet extends ArticleServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // response.getWriter().write("doGet from ArticlePublishServlet");
        try {
            ArticleService articleService = new ArticleService();
            articleService.publish(getUserId(request), Long.parseLong(request.getParameter("article")));
            response.sendRedirect(request.getContextPath() + "/");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

}
