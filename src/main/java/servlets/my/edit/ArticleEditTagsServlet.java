package servlets.my.edit;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import service.TagService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArticleEditTagsServlet extends ArticleServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

        String tagsStr = request.getReader().readLine();

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(tagsStr).getAsJsonArray();
        List<Long> tagsId = new ArrayList<>();
        for (int i = 0; i < jsonArray.size() ; i++) {
            tagsId.add(jsonArray.get(i).getAsLong());
        }
        TagService tagService = new TagService();
            tagService.updateTags(getUserId(request),Long.parseLong(request.getParameter("article"))
                ,tagsId);

        response.getWriter().write("Tags updated");
        } catch (SQLException e) {
            handleSQLExeption(request,response,e);
        }catch (Exception e2){
            handleExeption(request,response,e2);
        }


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{

            long articleId = Long.parseLong(request.getParameter("article"));
            long userId = getUserId(request);
            TagService tagService = new TagService();
            request.setAttribute("tags",tagService.getForArticle(articleId));
            request.setAttribute("articleId",articleId);
            request.getRequestDispatcher("/jsp/edit_article_tags.jsp").forward(request,response);

        } catch (SQLException e) {
            handleSQLExeption(request,response,e);
        }catch (Exception e2){
            handleExeption(request,response,e2);
        }


    }
}
