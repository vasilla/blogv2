package servlets.my.edit;

import model.User;
import service.ArticleService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ArticleEditCoauthorsServlet extends ArticleServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            long articleId = Long.parseLong(request.getParameter("article"));
            ArticleService articleService = new ArticleService();
            long coauthgorId = Long.parseLong(request.getReader().readLine());
            articleService.deleteCoauthor(articleId,coauthgorId);
            response.getWriter().write("done");
        } catch (SQLException e) {
            handleSQLExeption(request,response,e);
        }catch (Exception e2){
            handleExeption(request,response,e2);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            long articleId = Long.parseLong(request.getParameter("article"));
            ArticleService articleService = new ArticleService();
            List<User> coauthors = articleService.getCoauthor(articleId);
            request.setAttribute("coauthors",coauthors);
            request.setAttribute("articleId",articleId);
            request.getRequestDispatcher("/jsp/edit_article_coauthors.jsp").forward(request,response);
        } catch (SQLException e) {
            handleSQLExeption(request,response,e);
        }catch (Exception e2){
            handleExeption(request,response,e2);
        }




    }
}
