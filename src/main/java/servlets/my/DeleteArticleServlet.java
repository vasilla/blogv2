package servlets.my;

import service.ArticleService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

//TODO make filter
public class DeleteArticleServlet extends ArticleServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            long articleId = Long.parseLong(request.getParameter("article"));
            ArticleService articleService = new ArticleService();
            articleService.delete(articleId);
            response.sendRedirect(request.getContextPath()+"/my");
        }
        catch (SQLException e) {
           handleSQLExeption(request,response,e);
        }
        catch (Exception e){
            handleExeption(request,response,e);
        }

    }
}
