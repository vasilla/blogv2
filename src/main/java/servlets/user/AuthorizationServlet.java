package servlets.user;


import model.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

public class AuthorizationServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        User user;
//        try {
//            HttpSession session = request.getSession(true);
//            String login = request.getParameter("login");
//            String password = request.getParameter("password");
//            UserService userService = new UserService();
//            session.setAttribute("token",userService.authorize(login,password));
//            response.sendRedirect(request.getContextPath()+"/");
//        }catch (MyDatabaseException myDatabaseException){
//            response.getWriter().write("Some  Exception."+myDatabaseException.getMessage());
//        }
//        catch (SQLException sqlException){
//            response.getWriter().write("Some SQL Exception."+sqlException.getMessage());
//        } catch (GeneralSecurityException e){
//            response.getWriter().write("Some Token Exception."+e.getMessage());
//        }catch (Exception e){
//            System.out.println("error from AuthorizationServlet");
//            e.printStackTrace();
//        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/authorization.jsp").forward(request,response);
    }
}
