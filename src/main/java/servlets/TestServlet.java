package servlets;

import dao.article.ArticleDaoHibernate;
import dao.users.TestDao;
import dao.users.UserDaoHibernate;
import model.Article;
import model.Test;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArticleDaoHibernate articleDaoHibernate = new ArticleDaoHibernate();
        try {
//            List<Article> articles = articleDaoHibernate.getPublished(1,10);

            request.setAttribute("message",articleDaoHibernate.getCountOfPublished());
        }
        catch (Exception e){
            request.setAttribute("message",e.getMessage());
            e.printStackTrace();


        }



        request.getRequestDispatcher("/jsp/test.jsp").forward(request,response);
    }
}
