package servlets.admin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.Article;
import service.ArticleService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;

public class ArticlePageServlet extends ArticleServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try {

        String json = request.getReader().readLine();
        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            ArticleService articleService = new ArticleService();
        if (jsonObject.has("id")&&(jsonObject.has("page"))) {
            int page = jsonObject.get("page").getAsInt();
            if (jsonObject.has("reason")) {
                articleService.setBlocked(jsonObject.get("id").getAsLong(), getUserId(request),
                        jsonObject.get("reason").getAsString());
            }
            else {
                articleService.setUnBlocked(jsonObject.get("id").getAsLong());
            }
            response.getWriter().write("done");
        }
        else throw new Exception("Json incorrect");

        }  catch (SQLException e) {
            response.getWriter().write("SQLException");
        } catch (Exception e2) {
           response.getWriter().write("Exception");
        }


    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            ArticleService articleService = new ArticleService();
            List<Article> articles = articleService.getAllForAdmin(getPage(request,articleService.getCountOfPublishedForAdmin(),6),6);
            request.setAttribute("articles",articles);
            request.setAttribute("url_prefix","/admin/articles");
            request.getRequestDispatcher("/jsp/admin_articles.jsp").forward(request,response);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
