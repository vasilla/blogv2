package servlets.admin;

import model.Role;
import service.RoleService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class RolePageServlet extends ArticleServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            if ((request.getParameter("id") !=null)&&(request.getParameter("page") != null)) {
                RoleService roleService = new RoleService();
                if (request.getAttribute("delete")!= null){
                    roleService.delete(Long.parseLong(request.getParameter("id")));
                    response.sendRedirect(request.getContextPath()+"/admin/roles?page="+request.getParameter("page"));
                    return;
                }
                if (request.getParameter("name") != null) {
                    Role role = new Role();
                    role.setId(Long.parseLong(request.getParameter("id")));
                    role.setName(request.getParameter("name"));
                    role.setCanBlockUsers((request.getParameter("block_users")!=null));
                    role.setCanBlockArticle((request.getParameter("block_article")!=null));
                    role.setCanEditRoles((request.getParameter("edit_roles")!=null));
                    role.setCanEditPersonnel((request.getParameter("edit_personnel")!=null));
                    role.setCanEditTag((request.getParameter("edit_tags")!=null));
                    roleService.saveChanges(role);
                    response.sendRedirect(request.getContextPath()+"/admin/roles?page="+request.getParameter("page"));
                }

            }
            throw new Exception("incorrect request");

        }  catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            RoleService roleService = new RoleService();
            List<Role> roles = roleService.getForAdmin(getPage(request,roleService.getCount(),6),6);
            request.setAttribute("roles",roles);
            request.setAttribute("url_prefix","/admin/roles");
            request.getRequestDispatcher("/jsp/admin_roles.jsp").forward(request,response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
