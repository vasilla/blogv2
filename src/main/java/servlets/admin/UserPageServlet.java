package servlets.admin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.Role;
import model.User;
import service.RoleService;
import service.UserService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;

public class UserPageServlet extends ArticleServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            long userId = getUserId(request);
            String json = request.getReader().readLine();
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            UserService userService = new UserService();
            if (jsonObject.has("id")&&(jsonObject.has("page"))) {
                int page = jsonObject.get("page").getAsInt();
                if (jsonObject.has("reason")) {
                    userService.setBlocked(jsonObject.get("id").getAsLong(), userId,
                            jsonObject.get("reason").getAsString());

                }
                else {
                    userService.setUnBlocked(jsonObject.get("id").getAsLong());
                }
                response.getWriter().write(getUrl(request)+"/admin/users?page="+page);
            }
            else throw new Exception("Json incorrect");
        }  catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            long userId = getUserId(request);
            UserService userService = new UserService();
            List<User> users = userService.getForAdmin(getPage(request,userService.getCountOfUsers(),6),6);
            request.setAttribute("users",users);
            request.setAttribute("url_prefix","/admin/users");
            boolean canUpdateRole = true;
            request.setAttribute("can_update_role",canUpdateRole);
            RoleService roleService = new RoleService();
            if (canUpdateRole){
                List<Role> roles = roleService.getRoleNames();
                request.setAttribute("roles",roles);
                request.setAttribute("userId",userId);
            }
            request.getRequestDispatcher("/jsp/admin_users.jsp").forward(request,response);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
