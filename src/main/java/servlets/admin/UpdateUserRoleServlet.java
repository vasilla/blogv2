package servlets.admin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import service.UserService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UpdateUserRoleServlet extends ArticleServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            String json = request.getReader().readLine();
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();

            if (jsonObject.has("id")&&(jsonObject.has("page"))
                    &&(jsonObject.has("roleId"))) {
                int page = jsonObject.get("page").getAsInt();
                long userId = jsonObject.get("id").getAsLong();
                long roleId = jsonObject.get("roleId").getAsLong();
                UserService userService = new UserService();
                userService.setRight(userId,roleId);
                response.getWriter().write(getUrl(request)+"/admin/users?page="+page);
            }
            else throw new Exception("Json incorrect");
        }  catch (SQLException e) {
            handleSQLExeption(request,response,e);
        } catch (Exception e2) {
            handleExeption(request,response,e2);
        }

    }
}
