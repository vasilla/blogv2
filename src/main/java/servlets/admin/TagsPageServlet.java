package servlets.admin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.Tag;
import model.User;
import service.TagService;
import servlets.ArticleServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TagsPageServlet extends ArticleServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        try {
//
//            long userId = (long)request.getAttribute("userId");
//            String json = request.getReader().readLine();
//            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
//            TagService tagService = new TagService();
//            if ((jsonObject.has("id"))&&(jsonObject.has("page"))){
//                int page = jsonObject.get("page").getAsInt();
//                if(jsonObject.has("name")) {
//                    String name  = jsonObject.get("name").getAsString();
//                    Tag tag = new Tag();
//                    tag.setId(jsonObject.get("id").getAsLong());
//                    tag.setName(name);
//                    tag.setAuthor(new User(userId));
//                    tagService.saveChanges(tag);
//                }
//                else {
//                    tagService.delete(jsonObject.get("id").getAsLong());
//                }
//                response.getWriter().write(getUrl(request)+"/admin/tags?page="+page);
//            }
//            else throw new Exception("Json incorrect");
//        }  catch (SQLException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            TagService tagService = new TagService();
            List<Tag> tags = tagService.getForAdmin(getPage(request,tagService.getCount(),6),6);
            request.setAttribute("tags",tags);
            request.setAttribute("url_prefix","/admin/tags");
            request.getRequestDispatcher("/jsp/admin_tags.jsp").forward(request,response);
        }
        catch (Exception e) {
            handleExeption(request,response,e);
        }
    }
}
