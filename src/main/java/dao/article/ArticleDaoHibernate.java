package dao.article;

import model.Article;

import org.hibernate.Session;
import org.hibernate.query.Query;
import util.HibernateUtil;



import java.sql.SQLException;
import java.util.List;
import static util.SessionUtil.closeTransactionSession;
import static util.SessionUtil.getSession;
import static util.SessionUtil.openTransactionSession;

public class ArticleDaoHibernate {

    public List<Article> getPublished(int page,int maxPerPage) throws SQLException{
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            String hql = "From Article where published = true ";
            Query query = session.createQuery(hql);
            query.setFirstResult((page - 1) * maxPerPage);
            query.setMaxResults(maxPerPage);
            session.getTransaction().commit();
            List<Article> articles = query.getResultList();
            return query.getResultList();
        }
    }

    public long getCountOfPublished() throws SQLException{
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            String hql = "select count(*) from Article where published = true ";
            Query query = session.createQuery(hql);
            session.getTransaction().commit();
            return  (long)query.uniqueResult();
        }
    }
}
