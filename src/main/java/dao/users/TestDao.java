package dao.users;

import model.Test;
import org.hibernate.Session;

import static util.SessionUtil.closeTransactionSession;
import static util.SessionUtil.getSession;
import static util.SessionUtil.openTransactionSession;
public class TestDao {
    public void add(Test test){
        openTransactionSession();
        Session session = getSession();
        session.save(test);
        closeTransactionSession();
    }
}
