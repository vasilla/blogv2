package dao.users;

import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HibernateUtil;
import util.SessionUtil;

import static util.SessionUtil.closeTransactionSession;
import static util.SessionUtil.getSession;
import static util.SessionUtil.openTransactionSession;

public class UserDaoHibernate {

    public void createUser(User user){
        openTransactionSession();
        Session session = getSession();
        session.save(user);
        closeTransactionSession();

    }
}
