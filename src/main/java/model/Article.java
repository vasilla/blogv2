package model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "articles")
public class Article {
//    @NotNull
//    @Min(1)
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
//    @NotNull
//    @Min(1)
//    @Max(60)
    @Column(name = "title",length = 60)
    private String title;
//    @NotNull
//    @Min(1)
//    @Max(150)
    @Column(name = "description",length = 150)
    private String description;
//    @NotNull
    @Column(name = "published")
    private Boolean published;
    @Column(name = "published_date")
    private Timestamp publishedDate;
//    @NotNull
    @Column(name = "last_updated")
    private Date lastUpdated;
    @Column(name = "html",columnDefinition="TEXT")
    private String html;
//    @ManyToOne

    @JoinColumn(name = "user_id")
    @OneToOne
    @PrimaryKeyJoinColumn
    private User author;

    @Column(name = "image_name",columnDefinition="TEXT")
    private String imageName;




    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }


    public boolean isImageNameFromUrl() {
        return imageName.matches("^[a-z]{4,5}:\\/\\/[\\w,.,-,\\/,-]+((\\.jpg)|(\\.png))$");
    }



    public Article() {
    }


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Timestamp getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Timestamp publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
