package model;

import javax.persistence.*;

@Entity
@Table(name="blocked_article")
public class BlockedArticle {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @JoinColumn(name = "blocked_article_id")
    @OneToOne
    @PrimaryKeyJoinColumn
    private Article article;
    @Column (name = "reason",columnDefinition="TEXT")
    private String reason;

    @PrimaryKeyJoinColumn
    @OneToOne
    @JoinColumn (name = "user_id")
    private User author;

    public BlockedArticle() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
