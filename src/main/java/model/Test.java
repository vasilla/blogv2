package model;

import javax.persistence.*;

@Entity
@Table(name="test")
public class Test {
    @Id
    @Column(name = "id")
    @GeneratedValue()
    private Long id;

    @Column(name = "value")
    private String value;

    public Test() {
    }

    public Test(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
