package model;


import javax.persistence.*;

@Entity
@Table(name="tags")

public class Tag {
    @Id
    @Column(name = "id")
    @GeneratedValue()
    private Long id;

    @OneToOne()
    @PrimaryKeyJoinColumn
    @JoinColumn(name="user_id")
    private User author;

    @Column (name="name",length = 100)
    private String name;


    private boolean selected;

    public Tag() {
    }

    public Tag(long id,String name,boolean selected) {
        this.name = name;
        this.id = id;
        this.selected = selected;
    }

    public Tag(long id,String name ) {
        this.name = name;
        this.id = id;
    }

    public Tag(long id,String name,User author) {
        this.author = author;
        this.name = name;
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}
