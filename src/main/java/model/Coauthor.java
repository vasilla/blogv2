package model;

import javax.persistence.*;

@Entity
@Table(name="coauthors")
public class Coauthor {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @OneToOne
    @PrimaryKeyJoinColumn
    @JoinColumn(name = "user_id")
    private User user;

    @JoinColumn(name = "article_id")
    @OneToOne
    @PrimaryKeyJoinColumn
    private Article article;

    public Coauthor() {
    }

    public Coauthor(User user, Article article) {
        this.user = user;
        this.article = article;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
