package model;

import javax.persistence.*;

@Entity
@Table(name="blocked_user")
public class BlockedUser {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @JoinColumn(name = "blocked_user_id")
    @OneToOne
    @PrimaryKeyJoinColumn
    private User blockedUser;
    @Column (name = "reason",columnDefinition="TEXT")
    private String reason;
    @OneToOne
    @PrimaryKeyJoinColumn
    @JoinColumn (name = "author_id")
    private User author;

    public BlockedUser() {
        //Constructor must exist
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getBlockedUser() {
        return blockedUser;
    }

    public void setBlockedUser(User blockedUser) {
        this.blockedUser = blockedUser;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
