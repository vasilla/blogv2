package model;


import javax.persistence.*;

@Entity
@Table(name="roles")
public class Role {
    @Id
    @Column(name = "id")
    @GeneratedValue()
    private Long id;
    @Column(name = "name",length = 100)
    private String name;
    @Column(name = "can_edit_roles")
    private Boolean canEditRoles;
    @Column(name = "can_block_users")
    private Boolean canBlockUsers;
    @Column(name = "can_edit_personnel")
    private Boolean canEditPersonnel;
    @Column(name = "can_block_article")
    private Boolean canBlockArticle;
    @Column(name = "can_edit_tag")
    private Boolean canEditTag;


    public Role() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCanEditRoles() {
        return canEditRoles;
    }

    public void setCanEditRoles(Boolean canEditRoles) {
        this.canEditRoles = canEditRoles;
    }

    public Boolean getCanBlockUsers() {
        return canBlockUsers;
    }

    public void setCanBlockUsers(Boolean canBlockUsers) {
        this.canBlockUsers = canBlockUsers;
    }

    public Boolean getCanEditPersonnel() {
        return canEditPersonnel;
    }

    public void setCanEditPersonnel(Boolean canEditPersonnel) {
        this.canEditPersonnel = canEditPersonnel;
    }

    public Boolean getCanBlockArticle() {
        return canBlockArticle;
    }

    public void setCanBlockArticle(Boolean canBlockArticle) {
        this.canBlockArticle = canBlockArticle;
    }

    public Boolean getCanEditTag() {
        return canEditTag;
    }

    public void setCanEditTag(Boolean canEditTag) {
        this.canEditTag = canEditTag;
    }
}
