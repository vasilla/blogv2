package model;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {
//    @NotNull
//    @Min(1)
    @Id
    @Column(name = "id")
    @GeneratedValue()
    private Long id;
//    @NotNull
//    @Min(8)
//    @Max(30)
    @Column(name = "login",length = 50)
    private String login;
//    @NotNull
//    @Min(8)
//    @Max(30)
    @Column(name = "password",length = 50)
    private String password;
//    @NotNull
//    @Min(8)
//    @Max(30)
    @Column(name = "username",length = 100)
    private String username;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private Role role;


    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
